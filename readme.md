# React Web Boilerplate

React, Redux, Redux Thunk, Typescript, Webpack, React Atomic Design

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ yarn install
$ yarn start
```

### Todos

-

## License

MIT
