import React from 'react';

interface Props {}

const Home: React.FunctionComponent<Props> = (props: Props) => {
  return <div>Home page</div>;
};

export default Home;
